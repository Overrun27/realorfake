package com.RealOrFake;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InstructionActivity extends AppCompatActivity {
    private static Button start;
    WebView WebView;
    TextView t;
    private int h, w;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        t=(TextView) findViewById(R.id.textView2);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Raleway-ExtraBold.ttf");
        t.setTypeface(myCustomFont);

        WebView = (WebView) findViewById(R.id.webview);
        WebView.loadUrl("file:///android_asset/instruction.html");
        onClickButtonListener();
    }
    public void onClickButtonListener(){
        start = (Button)findViewById(R.id.button_start);
        start.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){
                        Intent intent = new Intent(getApplicationContext(),Game.class);
                        startActivity(intent);
                    }
                }
        );
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        updateSizeInfo();
    }

    private void updateSizeInfo() {
        RelativeLayout rlayout = (RelativeLayout) findViewById(R.id.content) ;
        w = rlayout.getWidth();
        h = rlayout.getHeight();
        Log.v("W-H", w+"-"+h);

    }
}
