package com.RealOrFake;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/*
    DOESN'T WORK
 */

public class LocalHighScore {

    PrintWriter writer;
    BufferedReader br;

    public LocalHighScore() {
        // see if file exists
        File f = new File("FakeOrReal_LocalHighScore.txt");
        if (!f.exists()) {
            Log.d("lhs", "DOESNT EXIST");
            try {
                writer = new PrintWriter("FakeOrReal_LocalHighScore.txt");
                writer.write("0 0");
                writer.flush();
            } catch (IOException e) { }
        }

        Log.d("output", "OKAY HERE 1");
    }

    public void changeScore(boolean guess) {
        int r[] = readScore();

        if (guess) r[0]++;
        r[1]++;

        try {
            writer = new PrintWriter("FakeOrReal_LocalHighScore.txt");
        } catch (FileNotFoundException e) { }

        writer.write(r[0] + " " + r[1]);
        writer.flush();
    }

    private int[] readScore() {
        String score = "";

        // read from FakeOrReal_LocalHighScore.txt file
        try {
            br = new BufferedReader(new FileReader("FakeOrReal_LocalHighScore.txt"));
            score = br.readLine();
            br.close();
        } catch (IOException e) { Log.d("output", "CANT READ"); }

        File f = new File("FakeOrReal_LocalHighScore.txt");
        if (!f.exists()) {
            Log.d("output", "NE OBSTAJA");
        }

        Log.d("output", "RESULT "+score);

        String tokens[] = score.split(" ");
        int result[] = new int[2];

        result[0] = Integer.parseInt(tokens[0]);    // number of correct guesses
        result[1] = Integer.parseInt(tokens[1]);    // number of total guesses

        return result;
    }
}
