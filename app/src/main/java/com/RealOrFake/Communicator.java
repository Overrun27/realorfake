package com.RealOrFake;

import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Communicator {
    private String picSource = "http://www.student.famnit.upr.si/~89151059/RealOrFake/getRandomImage.php";
    private String URLSource = "http://www.student.famnit.upr.si/~89151059/RealOrFake/";
    private ImageView display = null;
    private String displayedPath;

    private ArrayList<String> shown = null;

    public Communicator(ImageView display){
        this.display=display;
        shown = new ArrayList<>();
    }

    public void getRandomPic(){
        String picUrl = getRandomPicUrl();
        int counter = 0;

        while (shown.contains(picUrl)){
            picUrl = getRandomPicUrl();
            counter++;
            if(counter>20){
                counter=0;
                clearShown();
            }
        }

        shown.add(picUrl);
        swapPicture(picUrl);
    }

    private void swapPicture(String picUrl){
        try{
            URL url = new URL(picUrl);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.getDoInput();
            connection.connect();

            InputStream stream = connection.getInputStream();

            display.setImageBitmap(BitmapFactory.decodeStream(stream));
            displayedPath = picUrl;
        }catch(Exception e){
            Log.d("SWAPPICOUTPUT", "EXCEPTION");
            e.printStackTrace();
        }
    }

    private String getRandomPicUrl(){
        String ret = "";

        if (android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try{
                URL url = new URL(picSource);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();

                BufferedReader bfreader = new BufferedReader(new InputStreamReader(stream));
                ret = bfreader.readLine();

                connection.disconnect();

            }catch(Exception e){
                ret = "error";
                e.printStackTrace();
            }
        }
        return ret;
    }


    public String submitGuess(int real, int conf){
        String ret = "";
        if (android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            String path = displayedPath.split(URLSource)[1];

            String submit = URLSource+"guessImage.php?path="+path+"&guess="+real+" "+conf;
            Log.d("e", submit);

            try{
                URL url = new URL(submit);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                BufferedReader bfreader = new BufferedReader(new InputStreamReader(stream));

                ret = bfreader.readLine();

                connection.disconnect();

            }catch(Exception e){
                ret = "error";
                e.printStackTrace();
            }
        }

        return ret;
    }

    public void clearShown(){
        shown = new ArrayList<>();;
    }
    public int getShown(){
        return shown.size();
    }
}
