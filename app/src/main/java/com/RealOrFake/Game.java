package com.RealOrFake;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import io.github.controlwear.virtual.joystick.android.JoystickView;

public class Game extends AppCompatActivity {

    private TextView conf;
    private TextView stats;

    private ImageView mainImage;
    private ImageView realOrFake;

    private Button submit;

    private JoystickView joyS;
    private Communicator comm;

    //private LocalHighScore localScore;

    private int conf_level;
    private int guess;

    public int getConf(int x, int angle){
        double angleR=Math.toRadians((double)angle);
        double hip=((double)x)/50;
        int c=(int)((Math.sin(angleR)*hip)*100);
        //System.out.println(c);
        if(c>100){c=100;}
        return c;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_lay);

        final Context context = getApplicationContext();

        conf= (TextView) findViewById(R.id.conf);
        submit=(Button)findViewById(R.id.submit);
        joyS =(JoystickView) findViewById(R.id.joys);

        // me
        //localScore = new LocalHighScore();
        mainImage = (ImageView) findViewById(R.id.mainImage);
        comm = new Communicator(mainImage);
        comm.getRandomPic();

        realOrFake = (ImageView) findViewById(R.id.realOrFake);
        realOrFake.setVisibility(View.VISIBLE);
        realOrFake.setImageDrawable(null);  // dont show fake or real pic before touching joystick

        joyS.setOnMoveListener(new JoystickView.OnMoveListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onMove(int angle, final int strength) {
                if(angle!=0 && strength!=0){
                    if(angle>0 && angle<90){
                        realOrFake.setVisibility(View.VISIBLE);
                        submit.setVisibility(View.VISIBLE);

                        conf.setText("Confidance level: "+getConf(strength,angle));
                        conf.setVisibility(View.VISIBLE);

                        // swiping right, color the joystick to green and set top img to real
                        joyS.setButtonColor(getResources().getColor(R.color.joystickGreen));
                        realOrFake.setImageDrawable(getResources().getDrawable(R.drawable.real));

                        // set variables for confidence and whether we're guessing true of false as int
                        conf_level = getConf(strength,angle);
                        guess = 1; // REAL
                    }
                    else if(angle>90 && angle<180){
                        conf.setText("Confidance level: "+getConf(strength,angle));
                        submit.setVisibility(View.VISIBLE);

                        // swiping left, color the joystick to red and set top img to fake
                        joyS.setButtonColor(getResources().getColor(R.color.joystickRed));
                        realOrFake.setImageDrawable(getResources().getDrawable(R.drawable.fake));

                        // set variables for confidence and whether we're guessing ture of false as int
                        conf_level = getConf(strength,angle);
                        guess = 0; // FAKE
                    }
                }
            }
        });

        // when button is pressed, show stats
        stats = findViewById(R.id.statsText);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // NOTE: NEDELA
                //localScore.changeScore(true);

                // submit has been pressed
                // show stats, and change the buttons function to show next picture
                // also hide the joystick
                if (submit.getText().equals("SUBMIT")) {
                    submit.setText("NEXT");
                    stats.setVisibility(View.VISIBLE);
                    joyS.setVisibility(View.INVISIBLE);

                    // send the guess and confidence level
                    // get the stats and print them
                    handleStatistics();

                } else {
                    // button next has been pressed,
                    // hide statistics and reset joystick values, get new picture
                    // also show joystick again and hide the img at the top (fakeOrReal image)
                    submit.setText("SUBMIT");
                    conf.setText("Confidance level: 0");
                    stats.setVisibility(View.INVISIBLE);

                    joyS.resetButtonPosition();
                    joyS.setVisibility(View.VISIBLE);
                    joyS.setButtonColor(getResources().getColor(R.color.cyan));

                    realOrFake.setImageDrawable(null);

                    comm.getRandomPic();
                }
            }
        });
    }

    public static int getRF(int x){
        if(x<90) return 1;
        else return 0;
    }


    public void handleStatistics () {
        String returnedStats = comm.submitGuess(guess, conf_level);

        String values[] = returnedStats.split(" ");
        String outputText = "\n";

        outputText = outputText + values[0] + "% of people guessed right\n";
        outputText = outputText +  values[1] + " people took a guess\n";
        outputText = outputText + "Confidence of people that guessed right: " +
                values[2] + "\n";
        outputText = outputText + "Confidence of people that guessed wrong: " +values[2] + "\n";

        stats.setText(outputText);
    }
}

